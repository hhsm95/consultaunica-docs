
# 📈 Estatus de Consumo de API

- Método: `GET`
- URL de Consumo: `https://consultaunica.mx/api/v2/user-stats`

## Headers (obligado tener API Key)

```json
{
  "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Parámetros en URL

```sql
userEmail = user@email.com
productSlug = ifetel
includeGraph = true // opcional, por defecto: true
graphDate = 2022-12-01 // opcional, por defecto: today
```

### Ejemplo

```text
https://consultaunica.mx/api/v2/user-stats?userEmail=user@email.com&productSlug=ifetel&includeGraph=false&graphDate=2022-12-01
```

## ✅ Respuestas exitosas. HTTP 200

### Sin datos para graficar

Colocar el parámetro `includeGraph = false`

```json
{
    "usage": {
        "errors": 10,
        "remaining": 150,
        "successful": 300,
        "noResults": 50
    },
    "requests": null
}
```

### Con datos para graficar

Colocar el parámetro `includeGraph = true` y opcionalmente colocar una fecha en el campo `graphDate` (si de deja en blanco se usará la fecha actual), se tomarán los días del mes para graficar cuantas consultas se hicieron cada día (independientemente de sus status 200, 204, 500)

```json
{
    "usage": {
        "errors": 10,
        "remaining": 150,
        "successful": 300,
        "noResults": 50
    },
    "requests": {
        "y": [
            0,
            64,
            17,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        ],
        "x": [
            "2022-12-01",
            "2022-12-02",
            "2022-12-03",
            "2022-12-04",
            "2022-12-05",
            "2022-12-06",
            "2022-12-07",
            "2022-12-08",
            "2022-12-09",
            "2022-12-10",
            "2022-12-11",
            "2022-12-12",
            "2022-12-13",
            "2022-12-14",
            "2022-12-15",
            "2022-12-16",
            "2022-12-17",
            "2022-12-18",
            "2022-12-19",
            "2022-12-20",
            "2022-12-21",
            "2022-12-22",
            "2022-12-23",
            "2022-12-24",
            "2022-12-25",
            "2022-12-26",
            "2022-12-27",
            "2022-12-28",
            "2022-12-29",
            "2022-12-30",
            "2022-12-31"
        ]
    }
}
```

## ❌ Acceso no autorizado. HTTP 401

Comprueba que tu correo electrónico sea el mismo con el que adquiriste la API Key.

```json
{
    "message": "Unknown user"
}
```
