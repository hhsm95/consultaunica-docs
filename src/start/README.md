# Inicio

A continuación se describirá el consumo correcto de la API de Consulta.

## 📖 Información general

- Es necesario mencionar que este es un servicio REST y se consume a traves de un cliente HTTP. Esta pensado para utilizarse sobre un Backend, no se recomienda consumir directamente en Javascript sobre el navegador debido a tema de [CORS](https://developer.mozilla.org/es/docs/Web/HTTP/CORS).
- La autenticación a la API se realiza mediante el uso de una API Key la cual se debe enviar en `Header` del Request HTTP. El `Header` debe llamarse `X-API-KEY`.
- La API Key tiene una longitud de 48 caracteres hexadecimales.
- Para probar la API no es necesaria una API Key, solo debe omitirse el `Header` de `X-API-KEY`, la unica restricción de no tener una API Key es que hay un limite diario de 5 consultas diarias que se restablace cada día a las 12am hora del centro de México (UTC -6).
- En caso de decidirse por adquirir el servicio puede ver nuestros paquetes y precios en el siguiente enlace: <https://bit.ly/cu-services>
- Después de la compra estará recibiendo un correo con su API Key. Si pasan mas de 30 minutos desde su compra y no la ha recibido favor de comunicarse con nosotros mediante alguno de los medios disponibles descritos abajo.
- La URL base para consumir los servicios de la API es: `https://consultaunica.mx/api/v2/{service_name}` mediante el método `POST`.
- Se incluyen ejemplos avanzados de consumo en la documentación que tenemos para Postman: <https://bit.ly/cu-docs-pm>

## 💬 Medios de contacto

En caso de problemas o cualquier tema que quiera tratar, contamos con 3 medios de contacto disponibles:

- Mediante correo electrónico: [info@consultaunica.mx](mailto:info@consultaunica.mx?subject=Acerca%20de%20la%20Documentaci%C3%B3n%20de%20la%20API)
- WhatsApp: <https://bit.ly/cu-wa>
- Telegram: <https://bit.ly/cu-tg1>

## 🙌🏼 Servicios disponibles

|Servicio|Utilidad|Precios|
|-|-|-|
|Ifetel|Con un número de teléfono de 10 digitos devolverá que tipo de teléfono es (fijo o móvil), la compañía teléfonica, la lada y la ciudad de registro|<https://bit.ly/cu-srv-ifetel>|
|Repuve|Con un número de placa o de serie de un coche devolverá si tiene un reporte de Robo. Incluye el documento oficial expedido por Repuve|<https://bit.ly/cu-srv-repuve>|
|RFC|Mediante un RFC unicamente verifica si este RFC es válido ante el SAT. Atención: no devuelve ningún documento ni información personal|<https://bit.ly/cu-srv-rfc>|
|Contribuyente CFDI|Con el nuevo CFDI 4.0 ahora para facturar es necesario que el nombre del contribuyente, su RFC y su código postal sean los mismos que los que tiene el SAT, de no ser así la factura no será válida|<https://bit.ly/cu-srv-rfc-cfdi-40>|
|Estudios|Con el nombre completo o parcial de una persona verifica si esta cuenta con una cedula profesional registrada ante el Registro Nacional de Profesionistas. Atención: no devuelve ningún documento|<https://bit.ly/cu-srv-estudios>|
|IMSS|Mediante una CURP devuelve, en caso de existir, el NSS ademas de los documento probatorios. Una segunda variante puede devolver la constancia de vigencia de derechos y una tercer llamada puede devolver el documento de semanas cotizadas. Atención: Por lentitud del IMSS, los resultados pueden durar hasta 1 minuto|<https://bit.ly/cu-srv-imss>|
|Afore|Con una CURP se consulta si se cuenta con Afore así como su nombre y medios para ponerse en contacto.|<https://bit.ly/cu-srv-afore>|

## 🚀 Consumo de API

### 🌎 Códigos de respuesta

Códigos de respuesta que devuelven los endpoints de consumo:

|Código|Descripción|Descuenta un uso de API|
|-|-|-|
|`200`|✅ Todo estuvo bien, se devuelve contenido de la consulta|Si|
|`204`|⚠ No se encontraron resultados para los datos ingresados, no es un error|Si|
|`400`|❌ Hay un error con alguno de los datos ingresados|No|
|`404`|❌ No se encontró el servicio|No|
|`429`|❌ Limite alcanzado, se acabaron los usos de la API|No|
|`500`|❌ Error de un servicio externo|No|

Los errores `400`, `429` y `500` devuelve un JSON con la siguiente estructura:

```json
{
  "message": "Mensaje describiendo el error"
}
```

### 📈 Ver tu consumo

- [API para ver tu consumo aquí](../usage/README.md)

### 📞 Servicio de Ifetel

- [Ifetel aquí](../services/ifetel.md)

### 🚗 Servicio de Repuve

- [Repuve con Placa aquí](../services/repuve/by_plate.md)
- [Repuve con Serie aquí](../services/repuve/by_vin.md)

### 🏢 Servicio de RFC

- [Validación de RFC aquí](../services/sat/validation_rfc.md)
- [Validación de Contribuyente para CFDI 4.0 aquí](../services/sat/validation_contribuyete.md)

### 📚 Servicio de Estudios

- [Estudios con Nombre aquí](../services/studies/by_name.md)
- [Estudios con Cédula aquí](../services/studies/by_cedula.md)

### 🏥 Servicio de IMSS

- [NSS aquí](../services/imss/nss.md)
- [Vigencia de Derechos aquí](../services/imss/vd.md)
- [Semanas Cotizadas aquí](../services/imss/sc.md)

### 💰 Servicio de Afore

- [Afore aquí](../services/afore.md)
