---
home: true
heroImage: http://consultaunica.mx/static/favicon.png
tagline: Documentación de API de Consulta Única
actionText: Empezar →
actionLink: /start/
footer: Made by Hugo Sandoval with ❤️
---
