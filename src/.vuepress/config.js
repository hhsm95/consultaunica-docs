const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Consulta Única API Docs',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Inicio',
        link: '/start/',
      },
      {
        text: 'Estadísticas',
        link: '/usage/',
      },
      {
        text: 'Servicios',
        link: '/services/',
      },
      {
        text: 'Consulta Única',
        link: 'https://bit.ly/cu-from-docs'
      }
    ],
    sidebar: {
      '/start/': [
        {
          title: 'Inicio',
          collapsable: true,
          children: [
            '',
          ]
        }
      ],
      '/usage/': [
        {
          title: 'Estadísticas',
          collapsable: true,
          children: [
            '',
          ]
        }
      ],
      '/services/': [
        {
          title: 'Servicios',
          collapsable: true,
          children: [
            '',
            'ifetel',
            'repuve/by_plate',
            'repuve/by_vin',
            'imss/nss',
            'imss/vd',
            'imss/sc',
            'studies/by_name',
            'studies/by_cedula',
            'sat/validation_rfc',
            'sat/validation_contribuyente',
            'afore',
          ]
        }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
