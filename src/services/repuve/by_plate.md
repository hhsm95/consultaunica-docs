
# 🚗 Servicio de Repuve por Placa

- Método: `POST`
- URL de Consumo: `https://consultaunica.mx/api/v2/repuve`
- Compra de paquetes: <https://bit.ly/cu-srv-repuve>

## Headers (solo en caso de tener API Key)

```json
{
    "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Payload JSON de ejemplo (obligatorios)

### Con placa

```json
{
    "carPlate": "1A2B3C4"
}
```

## ✅ Respuesta exitosa. HTTP 200

```json
{
    "documentUrl": "https://consultaunica.mx/static/pdf/repuve/1A2B3C4D5E6F1A2B.pdf",
    "carStatus": {
        "pgj": {
        "message": "RECOVERED",
        "code": 3
        },
        "ocra": {
        "message": "OK",
        "code": 1
        }
    },
    "carInfo": {
        "origin": "INDONESIA",
        "complementData": "INDONESIA",
        "type": "DEPORTIVA",
        "brand": "YAMAHA",
        "model": "MT-03",
        "comments": "",
        "axisNumber": "",
        "vinNumber": "1A2B3C4D5E6F1A2B",
        "assemblyPlant": "INDONESIA",
        "doorsNumber": "0",
        "plate": "1A2B3C4",
        "platedIn": "SIN INFORMACION",
        "displacement": "123AB",
        "class": "MOTOCICLETA",
        "platedDate": null,
        "lastUpdatedDate": "2017-10-20",
        "registeredIn": "YAMAHA MOTOR DE MÉXICO S.A. DE C.V.",
        "year": 2018,
        "inscriptionDate": null,
        "version": "",
        "inscriptionNumber": "",
        "cylinders": "2 CILINDROS",
        "nciNumber": "1234ABC"
    }
}
```

Para saber si el coche es robado es necesario ingresar al objeto `carStatus.pgj.code` o `carStatus.ocra.code`, si el código es:

- `1`: El coche no tiene reporte de robo.
- `2`: El coche si cuenta con reporte de robo.
- `3`: Se reportó que se ha recuperado el coche.

## ⚠ No se encontró el coche. HTTP 204

Esto quiere decir que no hay registro, no se puede saber si ha tenido reporte de robo.

```json
// vacio
```

## ❌ Error de Placa. HTTP 400

La placa debe ser de 6 o 7 digitos.

```json
{
    "message": "{'carPlate': ['Length must be between 6 and 7.']}"
}
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
