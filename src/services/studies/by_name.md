
# 📚 Servicio de Estudios con Nombre (Cédulas SEP)

- Método: `POST`
- URL de Consumo: `https://consultaunica.mx/api/v2/studies?_v=2`
- Compra de paquetes: <https://bit.ly/cu-srv-estudios>

## Headers (solo en caso de tener API Key)

```json
{
    "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Payload JSON de ejemplo (obligatorios)

```json
{
    "name": "ANDRES MANUEL",
    "paternalName": "LOPEZ",
    "maternalName": "OBRADOR"
}
```

## Respuesta exitosa. HTTP 200

La respuesta es un `array` de `object`, esto quiere decir que pueden encontrarse multiples cédulas.

```json
[
    {
        "title": "LICENCIATURA EN CIENCIAS POLÍTICAS Y ADMINISTRACIÓN PÚBLICA",
        "name": "ANDRÉS MANUEL",
        "sex": "Hombre",
        "registrationEntity": "",
        "cedulaType": "C1",
        "institution": "UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO",
        "fullName": "ANDRÉS MANUEL LÓPEZ OBRADOR",
        "paternalName": "LÓPEZ",
        "maternalName": "OBRADOR",
        "cedula": "1418552",
        "registrationYear": 1989
    },
    {
        "title": "MAESTRIA DE EJEMPLO",
        "name": "ANDRÉS MANUEL",
        "sex": "Hombre",
        "registrationEntity": "",
        "cedulaType": "C1",
        "institution": "UNIVERSIDAD DE EJEMPLO",
        "fullName": "ANDRÉS MANUEL LÓPEZ OBRADOR",
        "paternalName": "LÓPEZ",
        "maternalName": "OBRADOR",
        "cedula": "1234567",
        "registrationYear": 2010
    }
]
```

## ⚠ No se encontró el teléfono. HTTP 204

```json
// vacio
```

## ❌ Error de datos. HTTP 400

El nombre y apellido paterno deben tener entre 1 y 50 carácteres.

```json
{
    "message": "{
        'paternalName': ['Length must be between 1 and 50.'],
        'name': ['Length must be between 1 and 50.']
    }"
}
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
