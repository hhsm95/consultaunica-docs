
# 📚 Servicio de Estudios con Cédula (Cédulas SEP)

- Método: `GET`
- URL de Consumo: `https://consultaunica.mx/api/v2/studies`
- Compra de paquetes: <https://bit.ly/cu-srv-estudios>

## Headers (solo en caso de tener API Key)

```json
{
  "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Parámetros en URL

```sql
cedula = 1418552
```

### Ejemplo

```text
https://consultaunica.mx/api/v2/studies?cedula=1418552
```

## Respuesta exitosa. HTTP 200

```json
{
    "title": "LICENCIATURA EN CIENCIAS POLÍTICAS Y ADMINISTRACIÓN PÚBLICA",
    "name": "ANDRÉS MANUEL",
    "sex": "Hombre",
    "registrationEntity": "",
    "cedulaType": "C1",
    "institution": "UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO",
    "fullName": "ANDRÉS MANUEL LÓPEZ OBRADOR",
    "paternalName": "LÓPEZ",
    "maternalName": "OBRADOR",
    "cedula": "1418552",
    "registrationYear": 1989
}
```

## ⚠ No se encontró el teléfono. HTTP 204

```json
// vacio
```

## ❌ Error de cédula. HTTP 400

La cédula debe tener entre 1 y 8 caracteres.

```json
{
    "message": "{'cedula': ['Length must be between 1 and 8.']}"
}
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
