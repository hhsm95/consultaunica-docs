
# 📞 Servicio de Ifetel

- Método: `POST`
- URL de Consumo: `https://consultaunica.mx/api/v2/ifetel`
- Compra de paquetes: <https://bit.ly/cu-srv-ifetel>

## Headers (solo en caso de tener API Key)

```json
{
  "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Payload JSON de ejemplo (obligatorios)

```json
{
  "phoneNumber": "5512345678"
}
```

## ✅ Respuestas exitosas. HTTP 200

### ☎ Teléfono fijo

```json
{
    "phoneCompany": "Telmex",
    "phoneType": "Fijo",
    "registrationCity": "CUAUHTEMOC",
    "lada": "55"
}
```

### 📱 Teléfono movil

```json
{
    "phoneCompany": "AT&T",
    "phoneType": "Móvil",
    "registrationCity": "CUAUHTEMOC",
    "lada": "55"
}
```

## ⚠ No se encontró el teléfono. HTTP 204

```json
// vacio
```

## ❌ Error de teléfono. HTTP 400

El teléfono no es de 10 dígitos

```json
{
    "message": "{'phoneNumber': ['Length must be 10.']}"
}
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
