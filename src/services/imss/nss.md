
# 🏥 Servicio de NSS del IMSS

- Método: `POST`
- URL de Consumo: `https://consultaunica.mx/api/v2/imss?_v=2`
- Compra de paquetes: <https://bit.ly/cu-srv-imss>

## Headers (solo en caso de tener API Key)

```json
{
    "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Payload JSON de ejemplo (obligatorios)

```json
{
    "type": "nss",
    "nss": {
        "curp": "LOOA531113HTCPBN07"
    },
    "userEmail": "amlo@gmail.com"
}
```

## Respuesta exitosa. HTTP 200

```json
{
    "personInfo": {
        "maternalName": "OBRADOR",
        "name": "ANDRES MANUEL",
        "paternalName": "LOPEZ",
        "sex": "H",
        "birthDate": "1953-11-13"
    },
    "nss": "1234567890"
}
```

## ⚠ No se encontró el teléfono. HTTP 204

```json
// vacio
```

## ❌ Error de Validación de Datos. HTTP 400

La CURP tiene un formato inválido

```json
{
    "message": "{'nss': {'curp': ['CURP does not match expected pattern']}}"
}
```

El correo tiene un formato inválido

```json
{
    "message": "{'userEmail': ['Not a valid email address.']}"
}
```

## ❌ Debe ir a una Subdelegación del IMSS. HTTP 500

Es común que los registros de algunas personas no este 100% digitalizado y es necesario que vayan al IMSS a registrar su información faltate.

```json
{
    "message": "Debe ir a una subdelgación del IMSS"
}
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
