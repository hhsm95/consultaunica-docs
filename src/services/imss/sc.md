# 🏥 Servicio de Semanas Cotizadas del IMSS

- Método: `POST`
- URL de Consumo: `https://consultaunica.mx/api/v2/imss?_v=2`
- Compra de paquetes: <https://bit.ly/cu-srv-imss>

## Headers (solo en caso de tener API Key)

```json
{
    "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Payload JSON de ejemplo (obligatorios)

```json
{
    "type": "sc",
    "vdSc": {
        "curp": "LOOA531113HTCPBN07",
        "nss": "1234567890"
    },
    "userEmail": "amlo@gmail.com"
}
```

## Respuesta exitosa. HTTP 200

```json
{
    "quotedWeeksInfo": {
        "quotedWeeksRecords": [
            {
                "endDate": null,
                "startDate": "2000-01-01",
                "employerRegistry": "Y123456789",
                "employerName": "EMPRESA SA DE CV",
                "salary": "1000",
                "employerEntity": "DISTRITO FEDERAL"
            }
        ],
        "quotedWeeks": 52
    },
    "document": "https://consultaunica.mx/static/pdf/imss/SC_1234567890.pdf"
}
```

## ⚠ No se encontró el teléfono. HTTP 204

```json
// vacio
```

## ❌ Error de CURP. HTTP 400

La CURP tiene un formato inválido

```json
{
    "message": "{'vdSc': {'curp': ['CURP does not match expected pattern']}}"
}
```

El NSS debe tener 11 dígitos

```json
{
    "message": "{'vdSc': {'nss': ['Length must be 11.']}}"
}
```

El correo tiene un formato inválido

```json
{
    "message": "{'userEmail': ['Not a valid email address.']}"
}
```

## ❌ Solo se puede consumir las semanas cotizadas 2 veces por día. HTTP 500

El IMSS así lo limita para cada consulta de la misma CURP y NSS.

```json
{
    "message": "Solo puede consultar 2 veces al día sus semanas cotizadas
"
}
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
