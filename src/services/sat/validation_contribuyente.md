
# 🏢 Servicio de Validación de Contribuyente para CFDI 4.0

- Método: `POST`
- URL de Consumo: `https://consultaunica.mx/api/v2/rfc-cfdi-40`
- Compra de paquetes: <https://bit.ly/cu-srv-rfc-cfdi-40>

## Headers (solo en caso de tener API Key)

```json
{
  "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Payload JSON de ejemplo (obligatorios)

El Payload es un `array` de `object`, esto quiere decir que pueden enviarse mutiples contribuyentes a revisión.

```json
[
    {
        "rfc": "LOOA531113FI5",
        "taxPayer": "ANDRES MANUEL LOPEZ OBRADOR",
        "zipCode": "06066"
    },
    {
        "rfc": "TSO991022PB6",
        "taxPayer": "TIENDAS SORIANA",
        "zipCode": "64610"
    }
]
```

## ✅ Respuestas exitosas. HTTP 200

```json
[
    {
        "success": false,
        "message": "El código postal no es correcto.",
        "taxPayer": "ANDRES MANUEL LOPEZ OBRADOR",
        "rfc": "LOOA531113FI5",
        "zipCode": "06066"
    },
    {
        "success": true,
        "message": "Los datos son válidos.",
        "taxPayer": "TIENDAS SORIANA",
        "rfc": "TSO991022PB6",
        "zipCode": "64610"
    }
]
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
