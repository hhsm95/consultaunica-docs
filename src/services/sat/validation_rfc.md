
# 🏢 Servicio de Validación de RFC

- Método: `POST`
- URL de Consumo: `https://consultaunica.mx/api/v2/rfc`
- Compra de paquetes: <https://bit.ly/cu-srv-rfc>

## Headers (solo en caso de tener API Key)

```json
{
  "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Payload JSON de ejemplo (obligatorios)

```json
{
  "rfc": "LOOA531113FI5"
}
```

## ✅ Respuesta exitosa. HTTP 200

### RFC válido

```json
{
  "isValid": true,
  "rfc": "LOOA531113FI5"
}
```

### RFC no válido

```json
{
  "isValid": false,
  "rfc": "LOOA531113XXX"
}
```

## ⚠ No se encontró el teléfono. HTTP 204

```json
// vacio
```

## ❌ Error de RFC. HTTP 400

El RFC tiene un formato inválido

```json
{
    "message": "{'rfc': ['RFC does not match expected pattern']}"
}
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
