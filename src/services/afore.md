
# 💰 Servicio de Afore

- Método: `POST`
- URL de Consumo: `https://consultaunica.mx/api/v2/afore`
- Compra de paquetes: <https://bit.ly/cu-srv-afore>

## Headers (solo en caso de tener API Key)

```json
{
  "X-API-KEY": "1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f1a2b3c4d5e6f"
}
```

## Payload JSON de ejemplo (obligatorios)

```json
{
    "curp": "LOOA531113HTCPBN07"
}
```

## ✅ Respuesta exitosa. HTTP 200

```json
{
    "website": "www.aforecoppel.com",
    "contactPhone": "8002677352",
    "afore": "AFORE COPPEL"
}
```

## ⚠ No se encontró Afore. HTTP 204

```json
// vacio
```

## ❌ Error de Afore. HTTP 400

Solo 1 consulta al día con la misma CURP

```json
{
    "message": "Only one request per day"
}
```

## ❌ Error desconocido. HTTP 500

```json
{
    "message": "Mensaje describiendo el error"
}
```
