# Listado de servicios

### 📞 Servicio de Ifetel

- [Ifetel aquí](./ifetel.md)

### 🚗 Servicio de Repuve

- [Repuve con Placa aquí](./repuve/by_plate.md)
- [Repuve con Serie aquí](./repuve/by_vin.md)

### 🏢 Servicio de RFC

- [Validación de RFC aquí](./sat/validation_rfc.md)
- [Validación de Contribuyente para CFDI 4.0 aquí](./sat/validation_contribuyete.md)

### 📚 Servicio de Estudios

- [Estudios con Nombre aquí](./studies/by_name.md)
- [Estudios con Cédula aquí](./studies/by_cedula.md)

### 🏥 Servicio de IMSS

- [NSS aquí](./imss/nss.md)
- [Vigencia de Derechos aquí](./imss/vd.md)
- [Semanas Cotizadas aquí](./imss/sc.md)

### 💰 Servicio de Afore

- [Afore aquí](./afore.md)
